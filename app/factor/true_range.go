/*
 * @Author: cedric.jia
 * @Date: 2021-08-22 16:05:14
 * @Last Modified by: cedric.jia
 * @Last Modified time: 2021-08-22 16:09:04
 */

package factor

import (
	"github.cedric1996.com/go-trader/app/models"
	"github.cedric1996.com/go-trader/app/util"
)

type TrueRangeFactor struct {
	calDate   string
	timestamp int64
	code      string
	period    int64
}

func NewTrueRangeFactor(calDate, code string, period int64) *TrueRangeFactor {
	return &TrueRangeFactor{
		calDate:   calDate,
		period:    period,
		code:      code,
		timestamp: util.ParseDate(calDate).Unix(),
	}
}

func (f *TrueRangeFactor) Run() error {
	if err := f.execute(); err != nil {
		return err
	}
	return nil
}

func (f *TrueRangeFactor) Clean() error {
	return models.RemoveTr(f.timestamp)
}

func (f *TrueRangeFactor) execute() error {
	return nil
}
